#!/usr/bin/env bash

# Cleanup std::string
sed 's/std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >/str/g' $1 > t1

# unsigned int -> unsign
sed 's/unsigned int/unsign/g' t1 > t2

# truncate the fullpath
sed 's/\/media\/arun\/Academics\/phd\/Bremen\/works\/Learnings\/approxComputing\/approx_synthesis_lts/...approx_synthesis_lts/g' t2 > t3

# remove the known std::
sed 's/std::tuple/tuple/g' t3 | sed 's/std::vector/vector/g' | \
    sed 's/std::allocator/allocator/g' | sed 's/std::map/map/g' | sed 's/std::list/list/g' | \
    sed 's/std::pair/pair/g' > t4

mv t4 ${1}.cleaned
printf "Cleaned up log = ${1}.cleaned\n"
\rm -f t1 t2 t3 t4



