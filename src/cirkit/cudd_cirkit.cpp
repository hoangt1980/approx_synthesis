/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cudd_cirkit.hpp"

#include <functional>
#include <iostream>
#include <sstream>

#include <boost/format.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/counting_range.hpp>

#include <cuddInt.h>

#include <cirkit/bitset_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

boost::multiprecision::uint256_t error_rate( const bdd_function_t& f, const bdd_function_t& fhat,
                                             const properties::ptr& settings,
                                             const properties::ptr& statistics )

{

  auto h = f.first.bddZero();

  for ( auto i = 0u; i < f.second.size(); ++i )
  {
    h |= f.second[i] ^ fhat.second[i];
  }

  std::stringstream s;
  s.precision(0);
  s << std::fixed << h.CountMinterm( f.first.ReadSize() );
  return boost::multiprecision::uint256_t( s.str() );

  //const auto counts = count_solutions( h.manager(), {h.getNode()} );
  //return get_count( h.manager(), h.getNode(), counts ) << h.getNode()->index;
}


}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
