// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : helloWorld_utils.cpp
// @brief  : Some utilities for helloWorld program.
//------------------------------------------------------------------------------

#include "helloWorld_utils.hpp"

namespace minikit
{
//---//------------------------------------------------------------------------------
//---void create_work_dir (const std::string &dir_name) {
//---  boost::filesystem::path p(dir_name);
//---  if ( boost::filesystem::exists (p) ) {
//---    std::cout << "[w] Directory \"" + dir_name + "\" exists. "
//---	      << "Contents will be overwritten"  << std::endl;
//---    return;
//---  }
//---  auto status = boost::filesystem::create_directory (p);
//---  if (!status)  std::cout << "[e] Cannot create work directory "
//---			  << "(Permissions? Disk full?)" << std::endl;
//---  assert (status);
//---}
//---
//---void delete_work_dir (const std::string &dir_name) {
//---  boost::filesystem::path p(dir_name);
//---  boost::filesystem::remove_all (p);
//---}
//---//------------------------------------------------------------------------------
//---// All the optimization steps in Yosys.
//---void optimize_with_yosys () {
//---  auto localdebug = false;
//---  Yosys::run_yosys_cmd("proc", localdebug);
//---  Yosys::run_yosys_cmd("opt", localdebug);
//---  // seg-fault with memory() command. some yosys bug.
//---  // Problem only wit libyosys, otherwise Ok with the standalone yosys.
//---  Yosys::run_yosys_cmd("memory_dff", localdebug);
//---  Yosys::run_yosys_cmd("opt_clean", localdebug);
//---  //Yosys::run_yosys_cmd("memory_share", localdebug); // Buggy in libyosys.
//---  Yosys::run_yosys_cmd("memory_collect", localdebug);
//---  Yosys::run_yosys_cmd("opt_clean", localdebug);
//---  Yosys::run_yosys_cmd("memory_map", localdebug);
//---  Yosys::run_yosys_cmd("opt_clean", localdebug);
//---  Yosys::run_yosys_cmd("flatten", localdebug);
//---  Yosys::run_yosys_cmd("clean", localdebug);
//---  Yosys::run_yosys_cmd("fsm", localdebug);
//---  Yosys::run_yosys_cmd("opt", localdebug);
//---  Yosys::run_yosys_cmd("flatten", localdebug);
//---  Yosys::run_yosys_cmd("clean", localdebug);
//---  Yosys::run_yosys_cmd("techmap", localdebug);
//---  Yosys::run_yosys_cmd("clean", localdebug);
//---  Yosys::run_yosys_cmd("opt", localdebug);
//---  Yosys::run_yosys_cmd("clean", localdebug);
//---}
//---//------------------------------------------------------------------------------
//---void run_abc_cmd (abc::Abc_Frame_t *frame, std::string cmd, bool debug) {
//---  if (debug) std::cout << "[i] Abc :: " << cmd << std::endl;
//---  auto status = abc::Cmd_CommandExecute ( frame, cmd.c_str() );
//---  // Interpretation of status is wrong. Its not a boolean for true or false.
//---  //if (debug && !status)  std::cout << "[e] Abc :: command " << cmd
//---  //				   << " failed " << std::endl;
//---}
//---//------------------------------------------------------------------------------
//---// Optimization step resyn2 in ABC - b, rw, rf, b, rw, rwz, b, rfz, rwz, b 
//---void optimize_with_abc (abc::Abc_Frame_t *frame) {
//---  auto localdebug = false;
//---  run_abc_cmd (frame, "strash", localdebug);
//---  run_abc_cmd (frame, "balance", localdebug);
//---  run_abc_cmd (frame, "rewrite", localdebug);
//---  run_abc_cmd (frame, "refactor", localdebug);
//---  run_abc_cmd (frame, "balance", localdebug);
//---  run_abc_cmd (frame, "rewrite", localdebug);
//---  run_abc_cmd (frame, "rewrite -z", localdebug);
//---  run_abc_cmd (frame, "balance", localdebug);
//---  run_abc_cmd (frame, "refactor -z", localdebug);
//---  run_abc_cmd (frame, "rewrite -z", localdebug);
//---  run_abc_cmd (frame, "balance", localdebug);
//---}
//---//------------------------------------------------------------------------------

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
