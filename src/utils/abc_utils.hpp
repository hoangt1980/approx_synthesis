// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : abc_utils.hpp
// @brief  : Utilities using direct ABC functions.
//           All functions are inlined wrappers for corresponding ABC functions.
//           with some extra error debugging and asserts.
//
// Note    : Must be directly included inside the program (or other routines)
//           There is NO related definitions (cpp file) because of inlining,
//           and the intention is not to be linked as part of libminikit.so
//------------------------------------------------------------------------------

#pragma once

#ifndef ABC_UTILS_HPP
#define ABC_UTILS_HPP

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <cassert>
#include <cstdlib>

#include <ext-libs/abc/abc_api.hpp>
#include <types/Path.hpp>
#include <types/Design.hpp>
#include <types/AbcTypes.hpp>
#include <utils/miter_utils.hpp>

namespace minikit
{

//------------------------------------------------------------------------------
inline bool is_abc_running () {
  return true; // TODO: Complete this.
}
//------------------------------------------------------------------------------
inline bool is_network_strashed (Network ntk) {
  return ( 1 == abc::Abc_NtkIsStrash (ntk) );
}
//------------------------------------------------------------------------------
inline bool is_topologically_sorted (Network ntk) {
  return ( 1 == abc::Abc_NtkIsTopo (ntk)  );
}
//------------------------------------------------------------------------------
inline Frame start_abc () {
  abc::Abc_Start();
  Frame frame = abc::Abc_FrameGetGlobalFrame();
  if ( !frame ) {
    abc::Abc_Stop();
    std::cout << "[e] Error: Not able to invoke the ABC frame" << std::endl;
    std::exit (-1);
  }
  return frame;
}
//------------------------------------------------------------------------------
inline void stop_abc () {
  abc::Abc_Stop();
}
//------------------------------------------------------------------------------
inline Network  read_blif_to_network (std::string iblif) {
  assert ( is_abc_running() );
  Network ntk = abc::Io_ReadBlif ( (char *)iblif.c_str(), 1 );
  ntk = abc::Abc_NtkToLogic (ntk); // Convert to Logic form.
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  return ntk;
}
//------------------------------------------------------------------------------
inline Network strash_network ( Network ntk ) {
  assert ( is_network_good(ntk) && "[e] Network failed!\n" );
  return  abc::Abc_NtkStrash (ntk, 1, 1, 0);
}
//------------------------------------------------------------------------------
inline void write_blif_from_network (std::string oblif, Network ntk) {
  assert ( is_network_good(ntk) );
  abc::Io_WriteBlif ( abc::Abc_NtkToNetlist (ntk),
		      (char *)oblif.c_str(), 0, 0, 0 );
}
//------------------------------------------------------------------------------
// Note: if inside a loop, think about scalability and memory.
inline void write_verilog_from_network (std::string oblif, Network ntk) {
  assert ( is_network_good(ntk) );
  auto aig_ntk = copy_network (ntk);
  aig_ntk = abc::Abc_NtkToNetlist(aig_ntk);
  abc::Abc_NtkToAig ( aig_ntk );
  abc::Io_WriteVerilog (aig_ntk, (char *)oblif.c_str());
  delete_network (aig_ntk);
}

//------------------------------------------------------------------------------
// Note: if inside a loop, think about scalability and memory.
// TODO: optimize, some of these steps are unnecessary.
// Note : always strash for network integrity (especially after approximation)
inline void write_aig_from_network (std::string oblif, Network ntk) {
  assert ( is_network_good(ntk) );
  abc::Io_WriteAiger (strash_network (ntk), (char *)oblif.c_str(), 1, 0, 0);

//---  auto aig_ntk = copy_network (ntk);
//---  aig_ntk = abc::Abc_NtkToNetlist(aig_ntk);
//---  abc::Abc_NtkToAig ( aig_ntk );
//---  strash_network (aig_ntk);
//---  abc::Io_WriteAiger (aig_ntk, (char *)oblif.c_str(), 1, 0, 0);
//---  delete_network (aig_ntk);
}

//------------------------------------------------------------------------------
inline std::vector <Object> get_po_list (Network ntk) {
  assert ( is_abc_running() );
  assert ( is_network_strashed (ntk) );
  std::vector <Object> po_list;
  Object  abc_pObj = nullptr;
  // This stuff below is the Abc_NtkForEachPo macro!
  for ( int i = 0;
	(i < abc::Abc_NtkPoNum (ntk)) && (((abc_pObj) = abc::Abc_NtkPo (ntk, i)), 1);
	i++ ) {
    po_list.emplace_back ( abc_pObj );
  }
  return po_list;
}
//------------------------------------------------------------------------------
inline std::vector <Object> get_pi_list (Network ntk) {
  assert ( is_abc_running() );
  assert ( is_network_strashed (ntk) );
  std::vector <Object> pi_list;
  Object  abc_pObj = nullptr;
  // This stuff below is the Abc_NtkForEachPi macro!
  for ( int i = 0; (i < abc::Abc_NtkPiNum (ntk))
	  && (((abc_pObj) = abc::Abc_NtkPi (ntk, i)), 1);
	i++ ) {
    pi_list.emplace_back ( abc_pObj );
  }
  return pi_list;
}
//------------------------------------------------------------------------------
inline void print_network_details (Network ntk, const std::string &banner) {
  assert ( is_abc_running() );
  assert ( is_network_good(ntk) );
  std::cout << "#--------------------------------------------#" << std::endl;
  std::cout << banner << std::endl;
  abc::Abc_NtkPrintIo (stdout, ntk, 1);
  abc::Abc_NtkPrintLevel (stdout, ntk, 1, 1, 1);
  std::cout << "#--------------------------------------------#" << std::endl;
}
//------------------------------------------------------------------------------
inline CutManager start_cut_manager (const int &max_cut_size) {
  CutManager cut_manager =
    abc::Abc_NtkManCutStart( max_cut_size, 100000, 100000, 100000 );
  assert ( cut_manager != nullptr
	   && "[e] Error: Not able invoke the cut manager!\n" );
  return cut_manager;
}
//------------------------------------------------------------------------------
inline void stop_cut_manager (CutManager cut_manager) { // TODO: to complete.
  
}
//------------------------------------------------------------------------------
inline int get_node_count (Network ntk) {
  assert ( is_network_good(ntk) && "[e] Network failed!\n" );
  return abc::Abc_NtkObjNumMax (ntk);
}
//------------------------------------------------------------------------------
inline Object get_ith_object (Network ntk, const int &i) {
  assert ( is_network_good(ntk) && "[e] Network failed!\n" );
  if ( get_node_count (ntk) < i ) std::cout << "What is going on?\n";
  return abc::Abc_NtkObj (ntk, i);
}
//------------------------------------------------------------------------------
inline bool is_node_internal (Object node) {
  if ( (node == nullptr) || !abc::Abc_ObjIsNode(node) ) return false;
  if ( abc::Abc_ObjIsPi(node) ) return false;
  if ( abc::Abc_ObjIsPo(node) ) return false;
  if ( abc::Abc_AigNodeIsConst(node) ) return false;
  return true;
}
//------------------------------------------------------------------------------
inline bool is_node_const (Object node) {
  if ( node == nullptr ) return false;
  if ( abc::Abc_AigNodeIsConst(node) ) return true;
  return false;
}
//------------------------------------------------------------------------------
inline bool is_node_good (Object  node) {
  if ( (node == nullptr) || !abc::Abc_ObjIsNode(node) ) return false;
  // skip persistant nodes
  if ( abc::Abc_NodeIsPersistant(node) ) return false;
  // skip the nodes with many fanouts // TODO: give a msg here.
  if ( abc::Abc_ObjFanoutNum(node) > 1000 ) return false;
  return true;
}
//------------------------------------------------------------------------------
inline ObjVector find_cut_for_node (CutManager cut_manager, Object  node) {
  assert (cut_manager != nullptr);
  assert (node != nullptr);
  ObjVector leaves = abc::Abc_NodeFindCut ( cut_manager, node, 0 );
  return leaves;
}
//------------------------------------------------------------------------------
inline void replace_node1_with_node2 (Network ntk,
				      Object  node1,
				      Object  node2) {
  assert ( is_network_good(ntk)  && "[e] Error: Network failed!\n" );
  assert ( is_object_good(node1) && "[e] Error: Nullptr in Node\n" );
  assert ( is_object_good(node2) && "[e] Error: Nullptr in Node\n" );
  abc::Abc_AigReplace ( (abc::Abc_Aig_t *)ntk->pManFunc, node1,  node2, 1 );
}
//------------------------------------------------------------------------------
inline bool iprove () {
  assert (false); // Not ready yet.
  //to Use int Abc_NtkIvyProve( Abc_Ntk_t ** ppNtk, void * pPars );
}
//------------------------------------------------------------------------------
inline Network  make_miter ( Network ntk1, Network ntk2 ) {
  assert (false); // Not ready yet.
  // check if networks are combinational.
  assert ( abc::Abc_NtkIsComb (ntk1) ); 
  assert ( abc::Abc_NtkIsComb (ntk2) );
  // make sure both networks are strashed.
  assert ( abc::Abc_NtkIsStrash (ntk1) );
  assert ( abc::Abc_NtkIsStrash (ntk2) );
  // check if PI/PO matches in both the ckt. what is this 4th flag, fComb?
  assert ( abc::Abc_NtkCompareSignals (ntk1, ntk2, 1, 1) );

  
  auto miter_ntk = copy_network ( ntk1 );
  
  return miter_ntk;
}
//------------------------------------------------------------------------------
inline std::string proper_vlog_token (const std::string &name) {
  auto found = name.find ("[");
  if (found != std::string::npos) return "\\" + name;
  else return name;
}
// Note: there is an ABC struct called "design". In future do not get confused.
inline Design make_design_from_network ( Network ntk ) {

  auto pi_list = get_pi_list (ntk);
  auto po_list = get_po_list (ntk);
  std::vector <Port> port_pi_list, port_po_list;
  for (auto &pi : pi_list) {
    port_pi_list.emplace_back (
      make_port ( proper_vlog_token (get_obj_name(pi) ), 1u) );
  }
  for (auto &po : po_list) {
    port_po_list.emplace_back (
      make_port ( proper_vlog_token (get_obj_name(po) ), 1u) );
  }

  auto design = make_design ( get_network_name (ntk), port_pi_list, port_po_list ); 
  return design;
}
//------------------------------------------------------------------------------
inline std::string get_path_string ( const Path &path ) {
  std::string nodes_str = "";
  for (auto &n : get_nodes_of_path (path)) {
    nodes_str = nodes_str + "  " + std::string ( get_obj_name (n) ) ;
  }
  std::string ret_str =  std::string ( get_obj_name (get_pi_of_path (path)) )
    + " <- " + nodes_str + " <- "
    + std::string ( get_obj_name (get_po_of_path (path)) );
  return ret_str;
}
//------------------------------------------------------------------------------
inline void print_paths (const std::vector <Path> &paths_list) {
  for (auto &path : paths_list) { 
    auto pi = get_obj_name ( get_pi_of_path (path) );
    auto po = get_obj_name ( get_po_of_path (path) );
    auto nodes = get_nodes_of_path (path);
    std::cout << "Path ::: " << po << " = ";
    for (auto &n : nodes) {
      std::cout << get_obj_name (n) << ", ";
    }
    std::cout << " :: " << pi << std::endl;
  }

}
//------------------------------------------------------------------------------
} // namespace minikit
#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
