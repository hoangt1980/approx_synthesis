// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : trial_abc.cpp
// @brief  : input is a verilog RTL output is a synthesised netlist, not
//           mapped to any technology lib and used internal pnemonics and
//           as such is useless.
//           Only to demonstrate the trial_abc, not good for anything else!
//------------------------------------------------------------------------------

#include "trial_abc.hpp"

using namespace minikit;

int main (int argc, char **argv) {
  using boost::program_options::value;
  program_options opts;
  
  std::string ifile = "kakakikikoo", ofile = "kakakikikoo";
  std::string top = "kakakikikoo";
  auto verbose = 1u;
  auto max_cut_size = 3;
  std::string check_port = "__NA__";
  auto limit = 1u;
  
  opts.add_options()
    ( "ifile,i",   value( &ifile ),  "Input Verilog RTL" )
    ( "ofile,o",   value( &ofile ),  "Output BLIF" )
    ( "top,t",     value( &top ),    "Name of the TOP Module" )
    ( "mmm,m",     value( &max_cut_size ),     "Test Input" )
    ( "check_port,p",  value( &check_port ),   "Port to Check" )
    ( "limit,l",       value( &limit ),        "Limit" )
    ( "verbose,v", value_with_default( &verbose ),  "Turn off verbose" )
    ;
  opts.parse( argc, argv );
  
  //-----------------------------------------------------------
  std::string work = "work";
  create_work_dir ( work ); // Create the work directory.
  auto work_path = work + "/"; // TODO: make independent of OS.
  //-----------------------------------------------------------
  // 1. Get the corresponding blifs.
  auto iblif = work_path + "input.blif";
  auto oblif = ofile; // TODO, for the timebeing.
  //ys_verilog_to_blif ( ifile, iblif, top );
  ys_verilog_to_blif ( ifile, iblif, top, true ); //keep yosys
  auto width = ys_get_port_width ( check_port );
  auto port_to_check = make_port (check_port, width);
  //-----------------------------------------------------------
  // 2. Fire up ABC 
  Frame frame  = start_abc();
  Network ntk  = read_blif_to_network (iblif);
  assert ( is_network_good (ntk) );
  //------------------------------------------------------------
  // READING Primary-Inputs & Primary-Outputs from network.
  auto pi_list = get_pi_list (ntk);
  auto po_list = get_po_list (ntk);
  
  std::cout << "PI :: ";
  for (auto &i : pi_list)  std::cout << get_obj_name (i) << ", ";
  std::cout << std::endl;

  std::cout << "PO :: ";
  for (auto &i : po_list)  std::cout << get_obj_name (i) << ", ";
  std::cout << std::endl;

  //------------------------------------------------------------
  // Do something with CUTS.
  auto cut_mgr = start_cut_manager (max_cut_size);
  // Get the first cut and then exit
  Object node = nullptr;
  auto total_nodes = get_node_count( ntk );
  auto ntk_appx = copy_network ( ntk );
  set_network_name ( ntk_appx, get_network_name (ntk) + "_appx" );
  
  print_network_details (ntk, "Original Network BEFORE:: ");
  print_network_details (ntk_appx, "Duplicated Network BEFORE :: ");

  auto constant1 = get_const1 (ntk_appx);
  
  for ( int i = 0; (i < get_obj_vector_size((ntk_appx)->vObjs)); i++ ) {
    node = abc::Abc_NtkObj (ntk_appx, i);
    if ( !is_node_good(node) ) continue;
    // stop if all nodes have been tried once
    if ( i >= total_nodes ) break;
    auto leaves = find_cut_for_node (cut_mgr, node);
    replace_node1_with_node2 (ntk_appx, node,  constant1);
    std::cout << "Node  " << node->Id
	      << " : Leaves = " << get_obj_vector_size (leaves)
	      << " : CutVolume = "  << get_volume_of_cut (node, leaves)
	      << std::endl;

    std::cout << "Number of cuts for the last node = "
	      << get_obj_vector_size (leaves) << std::endl;
    break;

  }

  ntk_appx = abc::Abc_NtkStrash (ntk_appx, 1, 1, 0);
  print_network_details (ntk, "Original Network AFTER:: ");
  print_network_details (ntk_appx, "Duplicated Network AFTER :: ");
  
  //------------------------------------------------------------
  // Always check the network integrity.
  if ( !check_network (ntk) )
    std::cout << "[e] Error. Original Network integrity failed..  :("
	      << std::endl;

  if ( !check_network (ntk_appx) )
    std::cout << "[e] Error. Approximated Network integrity failed..  :("
	      << std::endl;

  // Write out the results.
  std::string appx_file = work_path + oblif + "_appx.blif";
  std::string orig_file = work_path + oblif + "_orig.blif";
  write_blif_from_network (orig_file, ntk);
  write_blif_from_network (appx_file, ntk_appx);

  //------------------------------------------------------------
  assert ( is_topologically_sorted (ntk) );

  auto nTop = 2u;
  auto sorted_po_list = sort_po_on_path_lengths (po_list, nTop);
  std::cout << "Top " << nTop << " worst path POs are :: ";
  for (auto &po : sorted_po_list)
    std::cout << get_obj_name (po) << " ";
  std::cout << std::endl;

  
  std::vector <Path> paths_list;
  for (auto &po : po_list) {
    auto longest_path = get_longest_path_to_po (po);
    paths_list.emplace_back (longest_path);
  }
  
  for (auto &path : paths_list) {
    auto pi = get_obj_name ( get_pi_of_path (path) );
    auto po = get_obj_name ( get_po_of_path (path) );
    auto nodes = get_nodes_of_path (path);
    std::cout << "Path ::: " << po << " = ";
    for (auto &n : nodes) {
      std::cout << get_obj_name (n) << ", ";
    }
    std::cout << " :: " << pi << std::endl;
  }
  //------------------------------------------------------------
  // write out the miter logic up.
  auto design = make_design_from_network (ntk); // TODO: move this up.
  auto mvlog = work_path + "miter_tmp.v";
  write_worst_case_miter (design, port_to_check, "wc_miter", mvlog, limit);

  //------------------------------------------------------------
  // verify the miter.
  auto mblif = work_path + "miter_tmp.blif";
  std::cout << "here1 \n";
  //ys_verilog_to_blif ( mvlog, mblif, "wc_miter", true, true );
  ys_mixed_to_blif ( mvlog, orig_file, appx_file, mblif, "wc_miter", false );

  auto miter_ntk = read_blif_to_network (mblif);
  auto results = has_limit_crossed (miter_ntk);
  if ( results == SolverResult::YES ) std::cout << "yes" << std::endl;
  else if ( results == SolverResult::NO ) std::cout << "no" << std::endl;
  else if ( results == SolverResult::ERROR ) std::cout << "error" << std::endl;
  else std::cout << "timeout" << std::endl;
  
  delete_network ( miter_ntk );
  
  stop_abc();
  return 0;
}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
