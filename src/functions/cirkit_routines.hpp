// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cirkit_routines.hpp
// @brief  : Conversion between cirkit, abc and cudd datastructures.
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef CIRKIT_ROUTINES_HPP
#define CIRKIT_ROUTINES_HPP

#include <vector>

#include <utils/abc_utils.hpp>
#include <cirkit/bdd_utils.hpp>
#include <cirkit/read_aiger.hpp>
#include <cirkit/aig.hpp>
#include <cirkit/aig_utils.hpp>
#include <cirkit/simulate_aig.hpp>

namespace minikit {
cirkit::bdd_function_t network_to_bdd (Network &ntk, Cudd &mgr);  
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
