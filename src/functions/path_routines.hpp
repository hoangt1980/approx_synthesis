// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : path_routines.hpp
// @brief  : Algorithms on Paths. (longest-path, sort paths'list)
//           A Path is {PI, <set-of-nodes>, PO}
//------------------------------------------------------------------------------
#pragma once
#ifndef PATH_ROUTINES_HPP
#define PATH_ROUTINES_HPP

#include <cassert>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>

#include <ext-libs/abc/abc_api.hpp>
#include <types/Path.hpp>
#include <types/AbcTypes.hpp>
#include <utils/abc_utils.hpp>

namespace minikit {
  
Path get_longest_path_to_po (Object &po);
void sort_paths (std::vector <Path> & paths_list, unsigned &n);
void  merge_same_length_paths ( std::vector <Path> &paths_list );
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
