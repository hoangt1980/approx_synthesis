// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cut_routines.cpp
// @brief  : various routines for cut-set evaluation
//------------------------------------------------------------------------------

#include "cut_routines.hpp"

namespace minikit
{
  
//------------------------------------------------------------------------------
// Ported from abcResub.c :: Abc_CutVolumeCheck()
// Note: Need to be careful with graph traversing, side-effects - Unknown.
// Why are we not unmarking the marked nodes, after work? :-(
//
int get_volume_of_cut_rec ( Object pObj ) {
  // quit if the node is visited (or if it is a leaf)
  if ( abc::Abc_NodeIsTravIdCurrent(pObj) )  return 0;
  abc::Abc_NodeSetTravIdCurrent(pObj);
  if ( abc::Abc_ObjIsCi(pObj) )   // report the error
    std::cout << "[e] Error:: get_volume_of_cut_rec() The set of nodes is not a cut!"
	      << std::endl;
  // count the number of nodes in the leaves
  return (1 +
	  get_volume_of_cut_rec( abc::Abc_ObjFanin0(pObj) ) +
	  get_volume_of_cut_rec( abc::Abc_ObjFanin1(pObj) ) );
}


int get_volume_of_cut ( Object &node, ObjVector &leaves ) {
  assert (node != nullptr);
  assert (leaves != nullptr);
  if ( !is_node_good(node) ) return 0;
  Object pObj = nullptr;
  abc::Abc_NtkIncrementTravId ( node->pNtk );   // mark the leaves
  // macro Vec_PtrForEachEntry
  for ( int i = 0;
	( i < get_obj_vector_size (leaves) ) &&
	  (((pObj) = (Object)abc::Vec_PtrEntry (leaves, i)), 1);
	i++ ) {
    abc::Abc_NodeSetTravIdCurrent ( pObj );
  }
  // traverse the nodes starting from the given one and count them
  return get_volume_of_cut_rec ( node );
}
//------------------------------------------------------------------------------
std::vector <CutVolume> get_cut_volume_list ( CutManager &cut_mgr, Path &path ) {
  std::vector <CutVolume> cut_volume_list;
  for ( auto &node : get_nodes_of_path (path) ) {
    if ( !is_node_good(node) ) continue;
    if ( !is_node_internal(node) ) continue;
    auto leaves = find_cut_for_node (cut_mgr, node);
    auto volume = get_volume_of_cut ( node, leaves );
    cut_volume_list.emplace_back ( std::make_pair (node, volume) );
  }
  return cut_volume_list;
}

//------------------------------------------------------------------------------
bool sort_cut_volume_predicate ( const CutVolume &a, const CutVolume &b ) {
  if ( a.second > b.second ) return true;
  else return false;
}
void sort_cut_volume_list ( std::vector <CutVolume> &cut_volume_list ) {
  std::sort ( cut_volume_list.begin(), cut_volume_list.end(),
	      sort_cut_volume_predicate );
}

//------------------------------------------------------------------------------
bool rsort_cut_volume_predicate ( const CutVolume &a, const CutVolume &b ) {
  if ( a.second < b.second ) return true;
  else return false;
}
void reverse_sort_cut_volume_list ( std::vector <CutVolume> &cut_volume_list ) {
  std::sort ( cut_volume_list.begin(), cut_volume_list.end(),
	      rsort_cut_volume_predicate );
}
void randomize_cut_volume_list ( std::vector <CutVolume> &cut_volume_list ) {
  std::random_shuffle ( cut_volume_list.begin(), cut_volume_list.end() );
}

//------------------------------------------------------------------------------
void print_cut_volume_list ( CutManager &cut_mgr, Path &path, std::ofstream &rpt, 
			     const unsigned &debug ) {

  assert ( rpt.is_open() && "File stream rpt has to be opened before sending here!" );
  rpt << "Node     Leaves(count)    CutVolume  \n";
  if (debug > 0) std::cout << "Node     Leaves(count)    CutVolume  \n";

  for ( auto &node : get_nodes_of_path (path) ) {

    rpt << get_obj_name (node) << "      ";
    if (debug > 0) std::cout << get_obj_name (node) << "      ";
    auto leaves = find_cut_for_node (cut_mgr, node);
    
    rpt << "{ ";
    if (debug > 0) std::cout << "{ ";
    Object pObj = nullptr;
    for ( int i = 0;
	  ( i < get_obj_vector_size (leaves) ) &&
	    (((pObj) = (Object)abc::Vec_PtrEntry (leaves, i)), 1); 
	  i++ ) {
      rpt << get_obj_name(pObj) << " ";
      if (debug > 0) std::cout << get_obj_name(pObj) << " ";
    }
    rpt << "}      ";
    if (debug > 0) std::cout << "}      ";

    auto volume = get_volume_of_cut ( node, leaves );
    rpt << volume << "\n";
    if (debug > 0) std::cout << volume << "\n";

  }
}
  
//------------------------------------------------------------------------------
} // namespace minikit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
