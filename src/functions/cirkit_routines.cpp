// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cirkit_routines.cpp
// @brief  : Conversion between cirkit, abc and cudd datastructures.
//------------------------------------------------------------------------------

#include "cirkit_routines.hpp"

namespace minikit
{

cirkit::bdd_function_t network_to_bdd (Network &ntk, Cudd &mgr) {
  //assert (mgr != nullptr);
  assert (is_network_good(ntk));
  std::string aig_file = "/tmp/aig";
  write_aig_from_network (aig_file, ntk);
  auto aig = cirkit::aig_graph ();
  cirkit::read_aiger_binary (aig, aig_file, true);
  cirkit::bdd_simulator sim (mgr);
  const auto res = cirkit::simulate_aig( aig, sim );
  std::vector<BDD> fs;

  for ( const auto& output : cirkit::aig_info( aig ).outputs )
  {
    fs.push_back( res.at( output.first ) );
  }

  return {mgr, fs};

}

  
//------------------------------------------------------------------------------
} // namespace minikit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
